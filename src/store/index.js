import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const urlServer = "http://localhost:3000/";

export default new Vuex.Store({
  state: {
    productos: []
  },
  mutations: {
    setProductos(state, payload) {
      state.productos = payload;
    }
  },
  actions: {
    async cargarProductos({ commit }) {
      const peticion = await fetch(urlServer + "producto");
      //Obtener los datos de la petición
      const data = await peticion.json();
      console.log(data);
      commit('setProductos', data);
    },
    //Método para crear un producto
    async crearProducto({ commit }, objProducto) {
      await fetch(urlServer + "producto", {
        method: 'POST',
        //Indicar en la cabecera que se enviarán datos en formato json
        headers: {
          'Content-Type': 'application/json'
        },
        //Asignar el objeto js al cuerpo de la petición y convertirlo en un JSON
        body: JSON.stringify(objProducto)
      });
    },
    //Método para conectar con la api de eliminar producto
    async eliminarProducto({ commit }, obj) {
      await fetch(urlServer + "producto", {
        method: 'DELETE',
        //Indicar en la cabecera que se enviarán datos en formato json
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      })
    },
    //Método para conectar con la api de ACTUALIZAR producto
    async actualizarProducto({ commit }, objProducto) {
      await fetch(urlServer + "producto", {
        method: 'PUT',
        //Indicar en la cabecera que se enviarán datos en formato json
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objProducto)
      });
    }
  },
  modules: {
  }
})
